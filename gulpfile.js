var gulp = require('gulp')
var compass = require( 'gulp-for-compass' );

gulp.task( 'compass', function(){
    gulp.src('sass/*.scss')
        .pipe(compass({
            sassDir: 'sass',
            cssDir: 'css',
            force: true
        }));
})

gulp.task('default', ['compass'])
